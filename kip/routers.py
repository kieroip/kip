class DatabaseRouter(object):

    @staticmethod
    def db_for_read(model, **hints):
        # noinspection PyProtectedMember
        if model._meta.app_label == 'a2b':
            return 'a2b'
        return None
