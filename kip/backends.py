import whirlpool

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from a2b.models import CcUiAuthen, CcCard


class A2bAdminBackend(object):
    @staticmethod
    def authenticate(username=None, password=None):
        try:
            a2b_user = CcUiAuthen.objects.get(login=username, pwd_encoded=whirlpool.new(password).hexdigest())
        except ObjectDoesNotExist:
            return None
        else:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User(username=username, password=password)
                user.is_staff = True
                user.is_superuser = True
                user.email = a2b_user.email
                user.save()

            return user

    @staticmethod
    def get_user(user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class A2bCustomerBackend(object):
    @staticmethod
    def authenticate(username=None, password=None):
        try:
            a2b_customer = CcCard.objects.get(useralias=username, uipass=password)
        except ObjectDoesNotExist:
            return None
        else:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User(username=username, password=password)
                user.email = a2b_customer.email
                user.first_name = a2b_customer.firstname
                user.last_name = a2b_customer.lastname
                user.save()

            return user

    @staticmethod
    def get_user(user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
