# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class CdrMayo(models.Model):
    plan = models.CharField(db_column='PLAN', max_length=50)  # Field name made lowercase.
    nit = models.CharField(db_column='NIT', max_length=20)  # Field name made lowercase.
    cliente = models.CharField(db_column='CLIENTE', max_length=50)  # Field name made lowercase.
    fecha = models.DateTimeField(db_column='FECHA')  # Field name made lowercase.
    destino = models.CharField(db_column='DESTINO', max_length=40)  # Field name made lowercase.
    duracion_segundos = models.IntegerField(db_column='DURACION_SEGUNDOS', blank=True, null=True)  # Field name made lowercase.
    minutos_facturables = models.BigIntegerField(db_column='MINUTOS_FACTURABLES', blank=True, null=True)  # Field name made lowercase.
    tipo = models.CharField(db_column='TIPO', max_length=13, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CDR_MAYO'


class CcAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    datecreation = models.DateTimeField()
    active = models.CharField(max_length=1)
    login = models.CharField(unique=True, max_length=20)
    passwd = models.CharField(max_length=40, blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    language = models.CharField(max_length=5, blank=True, null=True)
    id_tariffgroup = models.IntegerField(blank=True, null=True)
    options = models.IntegerField()
    credit = models.DecimalField(max_digits=15, decimal_places=5)
    currency = models.CharField(max_length=3, blank=True, null=True)
    locale = models.CharField(max_length=10, blank=True, null=True)
    commission = models.DecimalField(max_digits=10, decimal_places=4)
    vat = models.DecimalField(max_digits=10, decimal_places=4)
    banner = models.TextField(blank=True, null=True)
    perms = models.IntegerField(blank=True, null=True)
    lastname = models.CharField(max_length=50, blank=True, null=True)
    firstname = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=40, blank=True, null=True)
    state = models.CharField(max_length=40, blank=True, null=True)
    country = models.CharField(max_length=40, blank=True, null=True)
    zipcode = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=70, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    company = models.CharField(max_length=50, blank=True, null=True)
    com_balance = models.DecimalField(max_digits=15, decimal_places=5)
    threshold_remittance = models.DecimalField(max_digits=15, decimal_places=5)
    bank_info = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_agent'


class CcAgentCommission(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_payment = models.BigIntegerField(blank=True, null=True)
    id_card = models.BigIntegerField()
    date = models.DateTimeField()
    amount = models.DecimalField(max_digits=15, decimal_places=5)
    description = models.TextField(blank=True, null=True)
    id_agent = models.IntegerField()
    commission_type = models.IntegerField()
    commission_percent = models.DecimalField(max_digits=10, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'cc_agent_commission'


class CcAgentSignup(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_agent = models.IntegerField()
    code = models.CharField(unique=True, max_length=30)
    id_tariffgroup = models.IntegerField()
    id_group = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_agent_signup'


class CcAgentTariffgroup(models.Model):
    id_agent = models.BigIntegerField()
    id_tariffgroup = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_agent_tariffgroup'
        unique_together = (('id_agent', 'id_tariffgroup'),)


class CcAlarm(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.TextField()
    periode = models.IntegerField()
    type = models.IntegerField()
    maxvalue = models.FloatField()
    minvalue = models.FloatField()
    id_trunk = models.IntegerField(blank=True, null=True)
    status = models.IntegerField()
    numberofrun = models.IntegerField()
    numberofalarm = models.IntegerField()
    datecreate = models.DateTimeField()
    datelastrun = models.DateTimeField()
    emailreport = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_alarm'


class CcAlarmReport(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cc_alarm_id = models.BigIntegerField()
    calculatedvalue = models.FloatField()
    daterun = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_alarm_report'


class CcAutorefillReport(models.Model):
    id = models.BigIntegerField(primary_key=True)
    daterun = models.DateTimeField()
    totalcardperform = models.IntegerField(blank=True, null=True)
    totalcredit = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_autorefill_report'


class CcBackup(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)
    path = models.CharField(max_length=255)
    creationdate = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_backup'


class CcBillingCustomer(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_card = models.BigIntegerField()
    date = models.DateTimeField()
    id_invoice = models.BigIntegerField()
    start_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_billing_customer'


class CcCall(models.Model):
    id = models.BigIntegerField(primary_key=True)
    sessionid = models.CharField(max_length=40)
    uniqueid = models.CharField(max_length=30)
    card_id = models.BigIntegerField()
    nasipaddress = models.CharField(max_length=30)
    starttime = models.DateTimeField()
    stoptime = models.DateTimeField()
    sessiontime = models.IntegerField(blank=True, null=True)
    calledstation = models.CharField(max_length=100)
    sessionbill = models.FloatField(blank=True, null=True)
    id_tariffgroup = models.IntegerField(blank=True, null=True)
    id_tariffplan = models.IntegerField(blank=True, null=True)
    id_ratecard = models.IntegerField(blank=True, null=True)
    id_trunk = models.IntegerField(blank=True, null=True)
    sipiax = models.IntegerField(blank=True, null=True)
    src = models.CharField(max_length=40)
    id_did = models.IntegerField(blank=True, null=True)
    buycost = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    id_card_package_offer = models.IntegerField(blank=True, null=True)
    real_sessiontime = models.IntegerField(blank=True, null=True)
    dnid = models.CharField(max_length=40)
    terminatecauseid = models.IntegerField(blank=True, null=True)
    destination = models.IntegerField(blank=True, null=True)
    a2b_custom1 = models.CharField(max_length=20, blank=True, null=True)
    a2b_custom2 = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_call'


class CcCallArchive(models.Model):
    id = models.BigIntegerField(primary_key=True)
    sessionid = models.CharField(max_length=40)
    uniqueid = models.CharField(max_length=30)
    card_id = models.BigIntegerField()
    nasipaddress = models.CharField(max_length=30)
    starttime = models.DateTimeField()
    stoptime = models.DateTimeField()
    sessiontime = models.IntegerField(blank=True, null=True)
    calledstation = models.CharField(max_length=30)
    sessionbill = models.FloatField(blank=True, null=True)
    id_tariffgroup = models.IntegerField(blank=True, null=True)
    id_tariffplan = models.IntegerField(blank=True, null=True)
    id_ratecard = models.IntegerField(blank=True, null=True)
    id_trunk = models.IntegerField(blank=True, null=True)
    sipiax = models.IntegerField(blank=True, null=True)
    src = models.CharField(max_length=40)
    id_did = models.IntegerField(blank=True, null=True)
    buycost = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    id_card_package_offer = models.IntegerField(blank=True, null=True)
    real_sessiontime = models.IntegerField(blank=True, null=True)
    dnid = models.CharField(max_length=40)
    terminatecauseid = models.IntegerField(blank=True, null=True)
    destination = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_call_archive'


class CcCallbackSpool(models.Model):
    id = models.BigIntegerField(primary_key=True)
    uniqueid = models.CharField(unique=True, max_length=40, blank=True, null=True)
    entry_time = models.DateTimeField()
    status = models.CharField(max_length=80, blank=True, null=True)
    server_ip = models.CharField(max_length=40, blank=True, null=True)
    num_attempt = models.IntegerField()
    last_attempt_time = models.DateTimeField()
    manager_result = models.CharField(max_length=60, blank=True, null=True)
    agi_result = models.CharField(max_length=60, blank=True, null=True)
    callback_time = models.DateTimeField()
    channel = models.CharField(max_length=60, blank=True, null=True)
    exten = models.CharField(max_length=60, blank=True, null=True)
    context = models.CharField(max_length=60, blank=True, null=True)
    priority = models.CharField(max_length=60, blank=True, null=True)
    application = models.CharField(max_length=60, blank=True, null=True)
    data = models.CharField(max_length=60, blank=True, null=True)
    timeout = models.CharField(max_length=60, blank=True, null=True)
    callerid = models.CharField(max_length=60, blank=True, null=True)
    variable = models.CharField(max_length=2000, blank=True, null=True)
    account = models.CharField(max_length=60, blank=True, null=True)
    async = models.CharField(max_length=60, blank=True, null=True)
    actionid = models.CharField(max_length=60, blank=True, null=True)
    id_server = models.IntegerField(blank=True, null=True)
    id_server_group = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_callback_spool'


class CcCallerid(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cid = models.CharField(unique=True, max_length=100)
    id_cc_card = models.BigIntegerField()
    activated = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'cc_callerid'


class CcCampaign(models.Model):
    name = models.CharField(unique=True, max_length=50)
    creationdate = models.DateTimeField()
    startingdate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    description = models.TextField(blank=True, null=True)
    id_card = models.BigIntegerField()
    secondusedreal = models.IntegerField(blank=True, null=True)
    nb_callmade = models.IntegerField(blank=True, null=True)
    status = models.IntegerField()
    frequency = models.IntegerField()
    forward_number = models.CharField(max_length=50, blank=True, null=True)
    daily_start_time = models.TimeField()
    daily_stop_time = models.TimeField()
    monday = models.IntegerField()
    tuesday = models.IntegerField()
    wednesday = models.IntegerField()
    thursday = models.IntegerField()
    friday = models.IntegerField()
    saturday = models.IntegerField()
    sunday = models.IntegerField()
    id_cid_group = models.IntegerField()
    id_campaign_config = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_campaign'


class CcCampaignConfig(models.Model):
    name = models.CharField(max_length=40)
    flatrate = models.DecimalField(max_digits=15, decimal_places=5)
    context = models.CharField(max_length=40)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_campaign_config'


class CcCampaignPhonebook(models.Model):
    id_campaign = models.IntegerField()
    id_phonebook = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_campaign_phonebook'
        unique_together = (('id_campaign', 'id_phonebook'),)


class CcCampaignPhonestatus(models.Model):
    id_phonenumber = models.BigIntegerField()
    id_campaign = models.IntegerField()
    id_callback = models.CharField(max_length=40)
    status = models.IntegerField()
    lastuse = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_campaign_phonestatus'
        unique_together = (('id_phonenumber', 'id_campaign'),)


class CcCampaignconfCardgroup(models.Model):
    id_campaign_config = models.IntegerField()
    id_card_group = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_campaignconf_cardgroup'
        unique_together = (('id_campaign_config', 'id_card_group'),)


class CcCard(models.Model):
    id = models.BigIntegerField(primary_key=True)
    creationdate = models.DateTimeField()
    firstusedate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    enableexpire = models.IntegerField(blank=True, null=True)
    expiredays = models.IntegerField(blank=True, null=True)
    username = models.CharField(unique=True, max_length=50)
    useralias = models.CharField(unique=True, max_length=50)
    uipass = models.CharField(max_length=50)
    credit = models.DecimalField(max_digits=15, decimal_places=5)
    tariff = models.IntegerField(blank=True, null=True)
    id_didgroup = models.IntegerField(blank=True, null=True)
    activated = models.CharField(max_length=1)
    status = models.IntegerField()
    lastname = models.CharField(max_length=50)
    firstname = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=40)
    state = models.CharField(max_length=40)
    country = models.CharField(max_length=40)
    zipcode = models.CharField(max_length=20)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=70)
    fax = models.FloatField()
    inuse = models.IntegerField(blank=True, null=True)
    simultaccess = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    lastuse = models.DateTimeField()
    nbused = models.IntegerField(blank=True, null=True)
    typepaid = models.IntegerField(blank=True, null=True)
    creditlimit = models.IntegerField(blank=True, null=True)
    voipcall = models.IntegerField(blank=True, null=True)
    sip_buddy = models.IntegerField(blank=True, null=True)
    iax_buddy = models.IntegerField(blank=True, null=True)
    language = models.CharField(max_length=5, blank=True, null=True)
    redial = models.CharField(max_length=50)
    runservice = models.IntegerField(blank=True, null=True)
    nbservice = models.IntegerField(blank=True, null=True)
    id_campaign = models.IntegerField(blank=True, null=True)
    num_trials_done = models.BigIntegerField(blank=True, null=True)
    vat = models.FloatField()
    servicelastrun = models.DateTimeField()
    initialbalance = models.DecimalField(max_digits=15, decimal_places=5)
    invoiceday = models.IntegerField(blank=True, null=True)
    autorefill = models.IntegerField(blank=True, null=True)
    loginkey = models.CharField(max_length=40)
    mac_addr = models.CharField(max_length=17)
    id_timezone = models.IntegerField(blank=True, null=True)
    tag = models.CharField(max_length=50)
    voicemail_permitted = models.IntegerField()
    voicemail_activated = models.SmallIntegerField()
    last_notification = models.DateTimeField(blank=True, null=True)
    email_notification = models.CharField(max_length=70)
    notify_email = models.SmallIntegerField()
    credit_notification = models.IntegerField()
    id_group = models.IntegerField()
    company_name = models.CharField(max_length=50)
    company_website = models.CharField(max_length=60)
    vat_rn = models.CharField(max_length=40, blank=True, null=True)
    traffic = models.BigIntegerField(blank=True, null=True)
    traffic_target = models.CharField(max_length=300)
    discount = models.DecimalField(max_digits=5, decimal_places=2)
    restriction = models.IntegerField()
    id_seria = models.IntegerField(blank=True, null=True)
    serial = models.BigIntegerField(blank=True, null=True)
    block = models.IntegerField()
    lock_pin = models.CharField(max_length=15, blank=True, null=True)
    lock_date = models.DateTimeField(blank=True, null=True)
    max_concurrent = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_card'


class CcCardArchive(models.Model):
    id = models.BigIntegerField(primary_key=True)
    creationdate = models.DateTimeField()
    firstusedate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    enableexpire = models.IntegerField(blank=True, null=True)
    expiredays = models.IntegerField(blank=True, null=True)
    username = models.CharField(max_length=50)
    useralias = models.CharField(max_length=50)
    uipass = models.CharField(max_length=50, blank=True, null=True)
    credit = models.DecimalField(max_digits=15, decimal_places=5)
    tariff = models.IntegerField(blank=True, null=True)
    id_didgroup = models.IntegerField(blank=True, null=True)
    activated = models.CharField(max_length=1)
    status = models.IntegerField(blank=True, null=True)
    lastname = models.CharField(max_length=50, blank=True, null=True)
    firstname = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=40, blank=True, null=True)
    state = models.CharField(max_length=40, blank=True, null=True)
    country = models.CharField(max_length=40, blank=True, null=True)
    zipcode = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=70, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    inuse = models.IntegerField(blank=True, null=True)
    simultaccess = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    lastuse = models.DateTimeField()
    nbused = models.IntegerField(blank=True, null=True)
    typepaid = models.IntegerField(blank=True, null=True)
    creditlimit = models.IntegerField(blank=True, null=True)
    voipcall = models.IntegerField(blank=True, null=True)
    sip_buddy = models.IntegerField(blank=True, null=True)
    iax_buddy = models.IntegerField(blank=True, null=True)
    language = models.CharField(max_length=5, blank=True, null=True)
    redial = models.CharField(max_length=50, blank=True, null=True)
    runservice = models.IntegerField(blank=True, null=True)
    nbservice = models.IntegerField(blank=True, null=True)
    id_campaign = models.IntegerField(blank=True, null=True)
    num_trials_done = models.BigIntegerField(blank=True, null=True)
    vat = models.FloatField()
    servicelastrun = models.DateTimeField()
    initialbalance = models.DecimalField(max_digits=15, decimal_places=5)
    invoiceday = models.IntegerField(blank=True, null=True)
    autorefill = models.IntegerField(blank=True, null=True)
    loginkey = models.CharField(max_length=40, blank=True, null=True)
    activatedbyuser = models.CharField(max_length=1)
    id_timezone = models.IntegerField(blank=True, null=True)
    tag = models.CharField(max_length=50, blank=True, null=True)
    voicemail_permitted = models.IntegerField()
    voicemail_activated = models.SmallIntegerField()
    last_notification = models.DateTimeField(blank=True, null=True)
    email_notification = models.CharField(max_length=70, blank=True, null=True)
    notify_email = models.SmallIntegerField()
    credit_notification = models.IntegerField()
    id_group = models.IntegerField()
    company_name = models.CharField(max_length=50, blank=True, null=True)
    company_website = models.CharField(max_length=60, blank=True, null=True)
    vat_rn = models.CharField(db_column='VAT_RN', max_length=40, blank=True, null=True)  # Field name made lowercase.
    traffic = models.BigIntegerField(blank=True, null=True)
    traffic_target = models.TextField(blank=True, null=True)
    discount = models.DecimalField(max_digits=5, decimal_places=2)
    restriction = models.IntegerField()
    mac_addr = models.CharField(max_length=17)

    class Meta:
        managed = False
        db_table = 'cc_card_archive'


class CcCardGroup(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=400, blank=True, null=True)
    users_perms = models.IntegerField()
    id_agent = models.IntegerField(blank=True, null=True)
    provisioning = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_card_group'


class CcCardHistory(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField(blank=True, null=True)
    datecreated = models.DateTimeField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_card_history'


class CcCardPackageOffer(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField()
    id_cc_package_offer = models.BigIntegerField()
    date_consumption = models.DateTimeField()
    used_secondes = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_card_package_offer'


class CcCardSeria(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True, null=True)
    value = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_card_seria'


class CcCardSubscription(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField(blank=True, null=True)
    id_subscription_fee = models.IntegerField(blank=True, null=True)
    startdate = models.DateTimeField()
    stopdate = models.DateTimeField()
    product_id = models.CharField(max_length=100, blank=True, null=True)
    product_name = models.CharField(max_length=100, blank=True, null=True)
    paid_status = models.IntegerField()
    last_run = models.DateTimeField()
    next_billing_date = models.DateTimeField()
    limit_pay_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_card_subscription'


class CcCardgroupService(models.Model):
    id_card_group = models.IntegerField()
    id_service = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_cardgroup_service'
        unique_together = (('id_card_group', 'id_service'),)


class CcCharge(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField()
    iduser = models.IntegerField()
    creationdate = models.DateTimeField()
    amount = models.FloatField()
    chargetype = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    id_cc_did = models.BigIntegerField(blank=True, null=True)
    id_cc_card_subscription = models.BigIntegerField(blank=True, null=True)
    cover_from = models.DateField(blank=True, null=True)
    cover_to = models.DateField(blank=True, null=True)
    charged_status = models.IntegerField()
    invoiced_status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_charge'


class CcConfig(models.Model):
    config_title = models.CharField(max_length=100, blank=True, null=True)
    config_key = models.CharField(max_length=100, blank=True, null=True)
    config_value = models.CharField(max_length=200, blank=True, null=True)
    config_description = models.CharField(max_length=500, blank=True, null=True)
    config_valuetype = models.IntegerField()
    config_listvalues = models.CharField(max_length=100, blank=True, null=True)
    config_group_title = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'cc_config'


class CcConfigGroup(models.Model):
    group_title = models.CharField(unique=True, max_length=64)
    group_description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'cc_config_group'


class CcConfiguration(models.Model):
    configuration_id = models.AutoField(primary_key=True)
    configuration_title = models.CharField(max_length=64)
    configuration_key = models.CharField(max_length=64)
    configuration_value = models.CharField(max_length=255)
    configuration_description = models.CharField(max_length=255)
    configuration_type = models.IntegerField()
    use_function = models.CharField(max_length=255, blank=True, null=True)
    set_function = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_configuration'


class CcCountry(models.Model):
    id = models.BigIntegerField(primary_key=True)
    countrycode = models.CharField(max_length=80)
    countryprefix = models.CharField(max_length=80)
    countryname = models.CharField(max_length=80)

    class Meta:
        managed = False
        db_table = 'cc_country'


class CcCurrencies(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    currency = models.CharField(unique=True, max_length=3)
    name = models.CharField(max_length=30)
    value = models.DecimalField(max_digits=12, decimal_places=5)
    lastupdate = models.DateTimeField()
    basecurrency = models.CharField(max_length=3)

    class Meta:
        managed = False
        db_table = 'cc_currencies'


class CcDid(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_didgroup = models.BigIntegerField()
    id_cc_country = models.IntegerField()
    activated = models.IntegerField()
    reserved = models.IntegerField(blank=True, null=True)
    iduser = models.BigIntegerField()
    did = models.CharField(unique=True, max_length=50)
    creationdate = models.DateTimeField()
    startingdate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    description = models.TextField(blank=True, null=True)
    secondusedreal = models.IntegerField(blank=True, null=True)
    billingtype = models.IntegerField(blank=True, null=True)
    fixrate = models.FloatField()
    connection_charge = models.DecimalField(max_digits=15, decimal_places=5)
    selling_rate = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_carrier_connect_charge = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_carrier_cost_min = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_retail_connect_charge = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_retail_cost_min = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_carrier_initblock = models.IntegerField()
    aleg_carrier_increment = models.IntegerField()
    aleg_retail_initblock = models.IntegerField()
    aleg_retail_increment = models.IntegerField()
    aleg_timeinterval = models.TextField(blank=True, null=True)
    aleg_carrier_connect_charge_offp = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_carrier_cost_min_offp = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_retail_connect_charge_offp = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_retail_cost_min_offp = models.DecimalField(max_digits=15, decimal_places=5)
    aleg_carrier_initblock_offp = models.IntegerField()
    aleg_carrier_increment_offp = models.IntegerField()
    aleg_retail_initblock_offp = models.IntegerField()
    aleg_retail_increment_offp = models.IntegerField()
    max_concurrent = models.IntegerField()
    inuse = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_did'


class CcDidDestination(models.Model):
    id = models.BigIntegerField(primary_key=True)
    destination = models.CharField(max_length=100, blank=True, null=True)
    priority = models.IntegerField()
    id_cc_card = models.BigIntegerField()
    id_cc_did = models.BigIntegerField()
    creationdate = models.DateTimeField()
    activated = models.IntegerField()
    secondusedreal = models.IntegerField(blank=True, null=True)
    voip_call = models.IntegerField(blank=True, null=True)
    validated = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_did_destination'


class CcDidUse(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField(blank=True, null=True)
    id_did = models.BigIntegerField()
    reservationdate = models.DateTimeField()
    releasedate = models.DateTimeField()
    activated = models.IntegerField(blank=True, null=True)
    month_payed = models.IntegerField(blank=True, null=True)
    reminded = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_did_use'


class CcDidgroup(models.Model):
    id = models.BigIntegerField(primary_key=True)
    didgroupname = models.CharField(max_length=50)
    creationdate = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_didgroup'


class CcEpaymentLog(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cardid = models.BigIntegerField()
    amount = models.CharField(max_length=50)
    vat = models.FloatField()
    paymentmethod = models.CharField(max_length=50)
    cc_owner = models.CharField(max_length=64, blank=True, null=True)
    cc_number = models.CharField(max_length=32, blank=True, null=True)
    cc_expires = models.CharField(max_length=7, blank=True, null=True)
    creationdate = models.DateTimeField()
    status = models.IntegerField()
    cvv = models.CharField(max_length=4, blank=True, null=True)
    credit_card_type = models.CharField(max_length=20, blank=True, null=True)
    currency = models.CharField(max_length=4, blank=True, null=True)
    transaction_detail = models.TextField(blank=True, null=True)
    item_type = models.CharField(max_length=30, blank=True, null=True)
    item_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_epayment_log'


class CcEpaymentLogAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    agent_id = models.BigIntegerField()
    amount = models.CharField(max_length=50)
    vat = models.FloatField()
    paymentmethod = models.CharField(max_length=50)
    cc_owner = models.CharField(max_length=64, blank=True, null=True)
    cc_number = models.CharField(max_length=32, blank=True, null=True)
    cc_expires = models.CharField(max_length=7, blank=True, null=True)
    creationdate = models.DateTimeField()
    status = models.IntegerField()
    cvv = models.CharField(max_length=4, blank=True, null=True)
    credit_card_type = models.CharField(max_length=20, blank=True, null=True)
    currency = models.CharField(max_length=4, blank=True, null=True)
    transaction_detail = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_epayment_log_agent'


class CcIaxBuddies(models.Model):
    id_cc_card = models.IntegerField()
    name = models.CharField(unique=True, max_length=80)
    accountcode = models.CharField(max_length=20)
    regexten = models.CharField(max_length=20)
    amaflags = models.CharField(max_length=7, blank=True, null=True)
    callerid = models.CharField(max_length=80)
    context = models.CharField(max_length=80)
    defaultip = models.CharField(db_column='DEFAULTip', max_length=50, blank=True, null=True)  # Field name made lowercase.
    host = models.CharField(max_length=50)
    language = models.CharField(max_length=2, blank=True, null=True)
    deny = models.CharField(max_length=95)
    permit = models.CharField(max_length=95, blank=True, null=True)
    mask = models.CharField(max_length=95)
    port = models.CharField(max_length=5)
    qualify = models.CharField(max_length=7, blank=True, null=True)
    secret = models.CharField(max_length=80)
    type = models.CharField(max_length=6)
    username = models.CharField(max_length=80)
    disallow = models.CharField(max_length=100)
    allow = models.CharField(max_length=100)
    regseconds = models.IntegerField()
    ipaddr = models.CharField(max_length=50)
    trunk = models.CharField(max_length=3, blank=True, null=True)
    dbsecret = models.CharField(max_length=40)
    regcontext = models.CharField(max_length=40)
    sourceaddress = models.CharField(max_length=50)
    mohinterpret = models.CharField(max_length=20)
    mohsuggest = models.CharField(max_length=20)
    inkeys = models.CharField(max_length=40)
    outkey = models.CharField(max_length=40)
    cid_number = models.CharField(max_length=40)
    sendani = models.CharField(max_length=10)
    fullname = models.CharField(max_length=40)
    auth = models.CharField(max_length=20)
    maxauthreq = models.CharField(max_length=15)
    encryption = models.CharField(max_length=20)
    transfer = models.CharField(max_length=10)
    jitterbuffer = models.CharField(max_length=10)
    forcejitterbuffer = models.CharField(max_length=10)
    codecpriority = models.CharField(max_length=40)
    qualifysmoothing = models.CharField(max_length=10)
    qualifyfreqok = models.CharField(max_length=10)
    qualifyfreqnotok = models.CharField(max_length=10)
    timezone = models.CharField(max_length=20)
    adsi = models.CharField(max_length=10)
    setvar = models.CharField(max_length=200)
    requirecalltoken = models.CharField(max_length=20)
    maxcallnumbers = models.CharField(max_length=10)
    maxcallnumbers_nonvalidated = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'cc_iax_buddies'


class CcInvoice(models.Model):
    id = models.BigIntegerField(primary_key=True)
    reference = models.CharField(unique=True, max_length=30, blank=True, null=True)
    id_card = models.BigIntegerField()
    date = models.DateTimeField()
    paid_status = models.IntegerField()
    status = models.IntegerField()
    title = models.CharField(max_length=50)
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'cc_invoice'


class CcInvoiceConf(models.Model):
    key_val = models.CharField(unique=True, max_length=50)
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'cc_invoice_conf'


class CcInvoiceItem(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_invoice = models.BigIntegerField()
    date = models.DateTimeField()
    price = models.DecimalField(max_digits=15, decimal_places=5)
    vat = models.DecimalField(db_column='VAT', max_digits=4, decimal_places=2)  # Field name made lowercase.
    description = models.TextField()
    id_ext = models.BigIntegerField(blank=True, null=True)
    type_ext = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_invoice_item'


class CcInvoicePayment(models.Model):
    id_invoice = models.BigIntegerField()
    id_payment = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_invoice_payment'
        unique_together = (('id_invoice', 'id_payment'),)


class CcIso639(models.Model):
    code = models.CharField(primary_key=True, max_length=2)
    name = models.CharField(unique=True, max_length=16)
    lname = models.CharField(max_length=16, blank=True, null=True)
    charset = models.CharField(max_length=16)

    class Meta:
        managed = False
        db_table = 'cc_iso639'


class CcLogpayment(models.Model):
    date = models.DateTimeField()
    payment = models.DecimalField(max_digits=15, decimal_places=5)
    card_id = models.BigIntegerField()
    id_logrefill = models.BigIntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    added_refill = models.SmallIntegerField()
    payment_type = models.IntegerField()
    added_commission = models.IntegerField()
    agent_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_logpayment'


class CcLogpaymentAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date = models.DateTimeField()
    payment = models.DecimalField(max_digits=15, decimal_places=5)
    agent_id = models.BigIntegerField()
    id_logrefill = models.BigIntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    added_refill = models.IntegerField()
    payment_type = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_logpayment_agent'


class CcLogrefill(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date = models.DateTimeField()
    credit = models.DecimalField(max_digits=15, decimal_places=5)
    card_id = models.BigIntegerField()
    description = models.TextField(blank=True, null=True)
    refill_type = models.IntegerField()
    added_invoice = models.IntegerField()
    agent_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_logrefill'


class CcLogrefillAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date = models.DateTimeField()
    credit = models.DecimalField(max_digits=15, decimal_places=5)
    agent_id = models.BigIntegerField()
    description = models.TextField(blank=True, null=True)
    refill_type = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_logrefill_agent'


class CcMessageAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_agent = models.IntegerField()
    message = models.TextField(blank=True, null=True)
    type = models.IntegerField()
    logo = models.IntegerField()
    order_display = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_message_agent'


class CcMonitor(models.Model):
    id = models.BigIntegerField(primary_key=True)
    label = models.CharField(max_length=50)
    dial_code = models.IntegerField(blank=True, null=True)
    description = models.CharField(max_length=250, blank=True, null=True)
    text_intro = models.CharField(max_length=250, blank=True, null=True)
    query_type = models.IntegerField()
    query = models.CharField(max_length=1000, blank=True, null=True)
    result_type = models.IntegerField()
    enable = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_monitor'


class CcNotification(models.Model):
    id = models.BigIntegerField(primary_key=True)
    key_value = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateTimeField()
    priority = models.IntegerField()
    from_type = models.IntegerField()
    from_id = models.BigIntegerField(blank=True, null=True)
    link_id = models.BigIntegerField(blank=True, null=True)
    link_type = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_notification'


class CcNotificationAdmin(models.Model):
    id_notification = models.BigIntegerField()
    id_admin = models.IntegerField()
    viewed = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_notification_admin'
        unique_together = (('id_notification', 'id_admin'),)


class CcOutboundCidGroup(models.Model):
    creationdate = models.DateTimeField()
    group_name = models.CharField(max_length=70)

    class Meta:
        managed = False
        db_table = 'cc_outbound_cid_group'


class CcOutboundCidList(models.Model):
    outbound_cid_group = models.IntegerField()
    cid = models.CharField(max_length=100, blank=True, null=True)
    activated = models.IntegerField()
    creationdate = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_outbound_cid_list'


class CcPackageGroup(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_package_group'


class CcPackageOffer(models.Model):
    id = models.BigIntegerField(primary_key=True)
    creationdate = models.DateTimeField()
    label = models.CharField(max_length=70)
    packagetype = models.IntegerField()
    billingtype = models.IntegerField()
    startday = models.IntegerField()
    freetimetocall = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_package_offer'


class CcPackageRate(models.Model):
    package_id = models.IntegerField()
    rate_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_package_rate'
        unique_together = (('package_id', 'rate_id'),)


class CcPackgroupPackage(models.Model):
    packagegroup_id = models.IntegerField()
    package_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_packgroup_package'
        unique_together = (('packagegroup_id', 'package_id'),)


class CcPaymentMethods(models.Model):
    payment_method = models.CharField(max_length=100)
    payment_filename = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'cc_payment_methods'


class CcPayments(models.Model):
    id = models.BigIntegerField(primary_key=True)
    customers_id = models.BigIntegerField()
    customers_name = models.CharField(max_length=200)
    customers_email_address = models.CharField(max_length=96)
    item_name = models.CharField(max_length=127, blank=True, null=True)
    item_id = models.CharField(max_length=127, blank=True, null=True)
    item_quantity = models.IntegerField()
    payment_method = models.CharField(max_length=32)
    cc_type = models.CharField(max_length=20, blank=True, null=True)
    cc_owner = models.CharField(max_length=64, blank=True, null=True)
    cc_number = models.CharField(max_length=32, blank=True, null=True)
    cc_expires = models.CharField(max_length=4, blank=True, null=True)
    orders_status = models.IntegerField()
    orders_amount = models.DecimalField(max_digits=14, decimal_places=6, blank=True, null=True)
    last_modified = models.DateTimeField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    orders_date_finished = models.DateTimeField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    currency_value = models.DecimalField(max_digits=14, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_payments'


class CcPaymentsAgent(models.Model):
    id = models.BigIntegerField(primary_key=True)
    agent_id = models.BigIntegerField()
    agent_name = models.CharField(max_length=200)
    agent_email_address = models.CharField(max_length=96)
    item_name = models.CharField(max_length=127, blank=True, null=True)
    item_id = models.CharField(max_length=127, blank=True, null=True)
    item_quantity = models.IntegerField()
    payment_method = models.CharField(max_length=32)
    cc_type = models.CharField(max_length=20, blank=True, null=True)
    cc_owner = models.CharField(max_length=64, blank=True, null=True)
    cc_number = models.CharField(max_length=32, blank=True, null=True)
    cc_expires = models.CharField(max_length=4, blank=True, null=True)
    orders_status = models.IntegerField()
    orders_amount = models.DecimalField(max_digits=14, decimal_places=6, blank=True, null=True)
    last_modified = models.DateTimeField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    orders_date_finished = models.DateTimeField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    currency_value = models.DecimalField(max_digits=14, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_payments_agent'


class CcPaymentsStatus(models.Model):
    status_id = models.IntegerField()
    status_name = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'cc_payments_status'


class CcPaypal(models.Model):
    payer_id = models.CharField(max_length=50, blank=True, null=True)
    payment_date = models.CharField(max_length=30, blank=True, null=True)
    txn_id = models.CharField(unique=True, max_length=30, blank=True, null=True)
    first_name = models.CharField(max_length=40, blank=True, null=True)
    last_name = models.CharField(max_length=40, blank=True, null=True)
    payer_email = models.CharField(max_length=55, blank=True, null=True)
    payer_status = models.CharField(max_length=30, blank=True, null=True)
    payment_type = models.CharField(max_length=30, blank=True, null=True)
    memo = models.TextField(blank=True, null=True)
    item_name = models.CharField(max_length=70, blank=True, null=True)
    item_number = models.CharField(max_length=70, blank=True, null=True)
    quantity = models.IntegerField()
    mc_gross = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    mc_fee = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    tax = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    mc_currency = models.CharField(max_length=3, blank=True, null=True)
    address_name = models.CharField(max_length=50)
    address_street = models.CharField(max_length=80)
    address_city = models.CharField(max_length=40)
    address_state = models.CharField(max_length=40)
    address_zip = models.CharField(max_length=20)
    address_country = models.CharField(max_length=30)
    address_status = models.CharField(max_length=30)
    payer_business_name = models.CharField(max_length=40)
    payment_status = models.CharField(max_length=30)
    pending_reason = models.CharField(max_length=50)
    reason_code = models.CharField(max_length=30)
    txn_type = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'cc_paypal'


class CcPhonebook(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True, null=True)
    id_card = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_phonebook'


class CcPhonenumber(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_phonebook = models.IntegerField()
    number = models.CharField(max_length=30)
    name = models.CharField(max_length=40, blank=True, null=True)
    creationdate = models.DateTimeField()
    status = models.SmallIntegerField()
    info = models.TextField(blank=True, null=True)
    amount = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_phonenumber'


class CcPrefix(models.Model):
    prefix = models.BigIntegerField(primary_key=True)
    destination = models.CharField(max_length=60)

    class Meta:
        managed = False
        db_table = 'cc_prefix'


class CcProvider(models.Model):
    provider_name = models.CharField(unique=True, max_length=30)
    creationdate = models.DateTimeField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_provider'


class CcRatecard(models.Model):
    idtariffplan = models.IntegerField()
    dialprefix = models.CharField(max_length=30)
    buyrate = models.DecimalField(max_digits=15, decimal_places=5)
    buyrateinitblock = models.IntegerField()
    buyrateincrement = models.IntegerField()
    rateinitial = models.DecimalField(max_digits=15, decimal_places=5)
    initblock = models.IntegerField()
    billingblock = models.IntegerField()
    connectcharge = models.DecimalField(max_digits=15, decimal_places=5)
    disconnectcharge = models.DecimalField(max_digits=15, decimal_places=5)
    stepchargea = models.DecimalField(max_digits=15, decimal_places=5)
    chargea = models.DecimalField(max_digits=15, decimal_places=5)
    timechargea = models.IntegerField()
    billingblocka = models.IntegerField()
    stepchargeb = models.DecimalField(max_digits=15, decimal_places=5)
    chargeb = models.DecimalField(max_digits=15, decimal_places=5)
    timechargeb = models.IntegerField()
    billingblockb = models.IntegerField()
    stepchargec = models.FloatField()
    chargec = models.FloatField()
    timechargec = models.IntegerField()
    billingblockc = models.IntegerField()
    startdate = models.DateTimeField()
    stopdate = models.DateTimeField()
    starttime = models.SmallIntegerField(blank=True, null=True)
    endtime = models.SmallIntegerField(blank=True, null=True)
    id_trunk = models.IntegerField(blank=True, null=True)
    musiconhold = models.CharField(max_length=100)
    id_outbound_cidgroup = models.IntegerField(blank=True, null=True)
    rounding_calltime = models.IntegerField()
    rounding_threshold = models.IntegerField()
    additional_block_charge = models.DecimalField(max_digits=15, decimal_places=5)
    additional_block_charge_time = models.IntegerField()
    tag = models.CharField(max_length=50, blank=True, null=True)
    disconnectcharge_after = models.IntegerField()
    is_merged = models.IntegerField(blank=True, null=True)
    additional_grace = models.IntegerField()
    minimal_cost = models.DecimalField(max_digits=15, decimal_places=5)
    announce_time_correction = models.DecimalField(max_digits=5, decimal_places=3)
    destination = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_ratecard'


class CcReceipt(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_card = models.BigIntegerField()
    date = models.DateTimeField()
    title = models.CharField(max_length=50)
    description = models.TextField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_receipt'


class CcReceiptItem(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_receipt = models.BigIntegerField()
    date = models.DateTimeField()
    price = models.DecimalField(max_digits=15, decimal_places=5)
    description = models.TextField()
    id_ext = models.BigIntegerField(blank=True, null=True)
    type_ext = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_receipt_item'


class CcRemittanceRequest(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_agent = models.BigIntegerField()
    amount = models.DecimalField(max_digits=15, decimal_places=5)
    type = models.IntegerField()
    date = models.DateTimeField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_remittance_request'


class CcRestrictedPhonenumber(models.Model):
    id = models.BigIntegerField(primary_key=True)
    number = models.CharField(max_length=50)
    id_card = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_restricted_phonenumber'


class CcServerGroup(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=60, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_server_group'


class CcServerManager(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_group = models.IntegerField(blank=True, null=True)
    server_ip = models.CharField(max_length=40, blank=True, null=True)
    manager_host = models.CharField(max_length=50, blank=True, null=True)
    manager_username = models.CharField(max_length=50, blank=True, null=True)
    manager_secret = models.CharField(max_length=50, blank=True, null=True)
    lasttime_used = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_server_manager'


class CcService(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    amount = models.FloatField()
    period = models.IntegerField()
    rule = models.IntegerField()
    daynumber = models.IntegerField()
    stopmode = models.IntegerField()
    maxnumbercycle = models.IntegerField()
    status = models.IntegerField()
    numberofrun = models.IntegerField()
    datecreate = models.DateTimeField()
    datelastrun = models.DateTimeField()
    emailreport = models.CharField(max_length=100)
    totalcredit = models.FloatField()
    totalcardperform = models.IntegerField()
    operate_mode = models.IntegerField(blank=True, null=True)
    dialplan = models.IntegerField(blank=True, null=True)
    use_group = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_service'


class CcServiceReport(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cc_service_id = models.BigIntegerField()
    daterun = models.DateTimeField()
    totalcardperform = models.IntegerField(blank=True, null=True)
    totalcredit = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_service_report'


class CcSipBuddies(models.Model):
    id_cc_card = models.IntegerField()
    name = models.CharField(unique=True, max_length=80)
    accountcode = models.CharField(max_length=20)
    regexten = models.CharField(max_length=20)
    amaflags = models.CharField(max_length=7, blank=True, null=True)
    callgroup = models.CharField(max_length=10, blank=True, null=True)
    callerid = models.CharField(max_length=80)
    canreinvite = models.CharField(max_length=20)
    context = models.CharField(max_length=80)
    defaultip = models.CharField(db_column='DEFAULTip', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dtmfmode = models.CharField(max_length=7)
    fromuser = models.CharField(max_length=80)
    fromdomain = models.CharField(max_length=80)
    host = models.CharField(max_length=50)
    insecure = models.CharField(max_length=20)
    language = models.CharField(max_length=2, blank=True, null=True)
    mailbox = models.CharField(max_length=50)
    md5secret = models.CharField(max_length=80)
    nat = models.CharField(max_length=30, blank=True, null=True)
    deny = models.CharField(max_length=95)
    permit = models.CharField(max_length=95, blank=True, null=True)
    mask = models.CharField(max_length=95)
    pickupgroup = models.CharField(max_length=10, blank=True, null=True)
    port = models.CharField(max_length=5)
    qualify = models.CharField(max_length=7, blank=True, null=True)
    restrictcid = models.CharField(max_length=1, blank=True, null=True)
    rtptimeout = models.CharField(max_length=3, blank=True, null=True)
    rtpholdtimeout = models.CharField(max_length=3, blank=True, null=True)
    secret = models.CharField(max_length=80)
    type = models.CharField(max_length=6)
    username = models.CharField(max_length=80)
    disallow = models.CharField(max_length=100)
    allow = models.CharField(max_length=100)
    musiconhold = models.CharField(max_length=100)
    regseconds = models.IntegerField()
    ipaddr = models.CharField(max_length=50)
    cancallforward = models.CharField(max_length=3, blank=True, null=True)
    fullcontact = models.CharField(max_length=80)
    setvar = models.CharField(max_length=100)
    regserver = models.CharField(max_length=20, blank=True, null=True)
    lastms = models.CharField(max_length=11, blank=True, null=True)
    defaultuser = models.CharField(max_length=40)
    auth = models.CharField(max_length=10)
    subscribemwi = models.CharField(max_length=10)
    vmexten = models.CharField(max_length=20)
    cid_number = models.CharField(max_length=40)
    callingpres = models.CharField(max_length=20)
    usereqphone = models.CharField(max_length=10)
    incominglimit = models.CharField(max_length=10)
    subscribecontext = models.CharField(max_length=40)
    musicclass = models.CharField(max_length=20)
    mohsuggest = models.CharField(max_length=20)
    allowtransfer = models.CharField(max_length=20)
    autoframing = models.CharField(max_length=10)
    maxcallbitrate = models.CharField(max_length=15)
    outboundproxy = models.CharField(max_length=40)
    rtpkeepalive = models.CharField(max_length=15)
    useragent = models.CharField(max_length=80, blank=True, null=True)
    callbackextension = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_sip_buddies'


class CcSpeeddial(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cc_card = models.BigIntegerField()
    phone = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    speeddial = models.IntegerField(blank=True, null=True)
    creationdate = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_speeddial'
        unique_together = (('id_cc_card', 'speeddial'),)


class CcStatusLog(models.Model):
    id = models.BigIntegerField(primary_key=True)
    status = models.IntegerField()
    id_cc_card = models.BigIntegerField()
    updated_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_status_log'


class CcSubscriptionService(models.Model):
    id = models.BigIntegerField(primary_key=True)
    label = models.CharField(max_length=200)
    fee = models.FloatField()
    status = models.IntegerField()
    numberofrun = models.IntegerField()
    datecreate = models.DateTimeField()
    datelastrun = models.DateTimeField()
    emailreport = models.CharField(max_length=100)
    totalcredit = models.FloatField()
    totalcardperform = models.IntegerField()
    startdate = models.DateTimeField()
    stopdate = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'cc_subscription_service'


class CcSubscriptionSignup(models.Model):
    id = models.BigIntegerField(primary_key=True)
    label = models.CharField(max_length=50)
    id_subscription = models.BigIntegerField(blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    enable = models.IntegerField()
    id_callplan = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_subscription_signup'


class CcSupport(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=70, blank=True, null=True)
    language = models.CharField(max_length=5)

    class Meta:
        managed = False
        db_table = 'cc_support'


class CcSupportComponent(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    id_support = models.SmallIntegerField()
    name = models.CharField(max_length=50)
    activated = models.SmallIntegerField()
    type_user = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_support_component'


class CcSystemLog(models.Model):
    iduser = models.IntegerField()
    loglevel = models.IntegerField()
    action = models.TextField()
    description = models.TextField(blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    tablename = models.CharField(max_length=255, blank=True, null=True)
    pagename = models.CharField(max_length=255, blank=True, null=True)
    ipaddress = models.CharField(max_length=255, blank=True, null=True)
    creationdate = models.DateTimeField()
    agent = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_system_log'


class CcTariffgroup(models.Model):
    iduser = models.IntegerField()
    idtariffplan = models.IntegerField()
    tariffgroupname = models.CharField(max_length=50)
    lcrtype = models.IntegerField()
    creationdate = models.DateTimeField()
    removeinterprefix = models.IntegerField()
    id_cc_package_offer = models.BigIntegerField()
    additional_code = models.CharField(max_length=10, blank=True, null=True)
    included_minutes = models.BigIntegerField()
    valor_minuto = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_tariffgroup'


class CcTariffgroupPlan(models.Model):
    idtariffgroup = models.IntegerField()
    idtariffplan = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_tariffgroup_plan'
        unique_together = (('idtariffgroup', 'idtariffplan'),)


class CcTariffplan(models.Model):
    iduser = models.IntegerField()
    tariffname = models.CharField(max_length=50)
    creationdate = models.DateTimeField()
    startingdate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    description = models.TextField(blank=True, null=True)
    id_trunk = models.IntegerField(blank=True, null=True)
    secondusedreal = models.IntegerField(blank=True, null=True)
    secondusedcarrier = models.IntegerField(blank=True, null=True)
    secondusedratecard = models.IntegerField(blank=True, null=True)
    reftariffplan = models.IntegerField(blank=True, null=True)
    idowner = models.IntegerField(blank=True, null=True)
    dnidprefix = models.CharField(max_length=30)
    calleridprefix = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'cc_tariffplan'
        unique_together = (('iduser', 'tariffname'),)


class CcTemplatemail(models.Model):
    id_language = models.CharField(max_length=20)
    mailtype = models.CharField(max_length=50, blank=True, null=True)
    fromemail = models.CharField(max_length=70, blank=True, null=True)
    fromname = models.CharField(max_length=70, blank=True, null=True)
    subject = models.CharField(max_length=130, blank=True, null=True)
    messagetext = models.CharField(max_length=3000, blank=True, null=True)
    messagehtml = models.CharField(max_length=3000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_templatemail'
        unique_together = (('mailtype', 'id_language'),)


class CcTicket(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_component = models.SmallIntegerField()
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    priority = models.SmallIntegerField()
    creationdate = models.DateTimeField()
    creator = models.BigIntegerField()
    status = models.SmallIntegerField()
    creator_type = models.IntegerField()
    viewed_cust = models.IntegerField()
    viewed_agent = models.IntegerField()
    viewed_admin = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_ticket'


class CcTicketComment(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date = models.DateTimeField()
    id_ticket = models.BigIntegerField()
    description = models.TextField(blank=True, null=True)
    creator = models.BigIntegerField()
    creator_type = models.IntegerField()
    viewed_cust = models.IntegerField()
    viewed_agent = models.IntegerField()
    viewed_admin = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cc_ticket_comment'


class CcTimezone(models.Model):
    gmtzone = models.CharField(max_length=255, blank=True, null=True)
    gmttime = models.CharField(max_length=255, blank=True, null=True)
    gmtoffset = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'cc_timezone'


class CcTrunk(models.Model):
    id_trunk = models.AutoField(primary_key=True)
    trunkcode = models.CharField(max_length=50, blank=True, null=True)
    trunkprefix = models.CharField(max_length=20, blank=True, null=True)
    providertech = models.CharField(max_length=20)
    providerip = models.CharField(max_length=80)
    removeprefix = models.CharField(max_length=20, blank=True, null=True)
    secondusedreal = models.IntegerField(blank=True, null=True)
    secondusedcarrier = models.IntegerField(blank=True, null=True)
    secondusedratecard = models.IntegerField(blank=True, null=True)
    creationdate = models.DateTimeField()
    failover_trunk = models.IntegerField(blank=True, null=True)
    addparameter = models.CharField(max_length=120, blank=True, null=True)
    id_provider = models.IntegerField(blank=True, null=True)
    inuse = models.IntegerField(blank=True, null=True)
    maxuse = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    if_max_use = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_trunk'


class CcUiAuthen(models.Model):
    userid = models.BigIntegerField(primary_key=True)
    login = models.CharField(unique=True, max_length=50)
    pwd_encoded = models.CharField(max_length=250)
    groupid = models.IntegerField(blank=True, null=True)
    perms = models.IntegerField(blank=True, null=True)
    confaddcust = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    direction = models.CharField(max_length=80, blank=True, null=True)
    zipcode = models.CharField(max_length=20, blank=True, null=True)
    state = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=30, blank=True, null=True)
    fax = models.CharField(max_length=30, blank=True, null=True)
    datecreation = models.DateTimeField()
    email = models.CharField(max_length=70, blank=True, null=True)
    country = models.CharField(max_length=40, blank=True, null=True)
    city = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_ui_authen'


class CcVersion(models.Model):
    version = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'cc_version'


class CcVoucher(models.Model):
    id = models.BigIntegerField(primary_key=True)
    creationdate = models.DateTimeField()
    usedate = models.DateTimeField()
    expirationdate = models.DateTimeField()
    voucher = models.CharField(unique=True, max_length=50)
    usedcardnumber = models.CharField(max_length=50, blank=True, null=True)
    tag = models.CharField(max_length=50, blank=True, null=True)
    credit = models.FloatField()
    activated = models.CharField(max_length=1)
    used = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc_voucher'
