# coding=utf-8
from django import forms

from a2b.models import CcCard
from kv.constants import CALL_STATUS
from kv.models import SmsPlanCustomer, InternetPlanCustomer, PbxVirtual


class SmsPlanCustomerForm(forms.ModelForm):
    customers = [c.values() for c in CcCard.objects.all().values('id', 'company_name')]
    customer = forms.ChoiceField(choices=customers)

    class Meta:
        model = SmsPlanCustomer
        fields = ['plan', 'customer', 'left']


class InternetPlanCustomerForm(forms.ModelForm):
    customers = [c.values() for c in CcCard.objects.all().values('id', 'company_name')]
    customer = forms.ChoiceField(choices=customers)

    class Meta:
        model = InternetPlanCustomer
        fields = ['plan', 'customer']


class PbxVirtualForm(forms.ModelForm):
    customers = [c.values() for c in CcCard.objects.all().values('id', 'company_name')]
    customer = forms.ChoiceField(choices=customers)

    class Meta:
        model = PbxVirtual
        fields = ['code', 'customer', 'channels', 'is_active']


class CDRFilterForm(forms.Form):
    statuses = [
        ('', 'Todos'),
        (CALL_STATUS.ANSWER, 'Contestada'),
        (CALL_STATUS.BUSY, 'Ocupado'),
        (CALL_STATUS.NOANSWER, 'No contestada'),
        (CALL_STATUS.CANCEL, 'Cancelada'),
        (CALL_STATUS.CONGESTION, 'Congestión'),
        (CALL_STATUS.CHANUNAVAIL, 'No disponible'),
    ]

    date_from = forms.DateField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    date_until = forms.DateField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

    destination = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = forms.ChoiceField(required=False, choices=statuses, widget=forms.Select(attrs={'class': 'form-control'}))
