from json import dumps

import requests
from django.core.management.base import NoArgsCommand
from django.utils.timezone import now

from kv.constants import SMS_STATUS
from kv.models import Log, SmsPlanCustomer, Port
from requests.exceptions import ConnectionError


class Command(NoArgsCommand):
    help = 'Update SMS message statuses in Queue'

    def handle_noargs(self, **options):
        session = requests.Session()
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        for sms_sending in Log.objects.select_related().filter(status=SMS_STATUS.SENDING):
            data = dict(number=[sms_sending.phone], user_id=[sms_sending.sms_id])
            session.auth = (sms_sending.gateway.username, sms_sending.gateway.password)

            url = 'http://{}/api/query_sms_result'.format(sms_sending.gateway.hostname)
            try:
                response = session.post(url, data=dumps(data), headers=headers)
            except ConnectionError:
                # TODO: Update Queue
                continue

            if response.status_code != requests.codes.ok:
                # TODO: Report error to gateway
                continue

            if response.json()['error_code'] == 413:
                Log.objects.filter(id=sms_sending.id).update(status=SMS_STATUS.FAILED, updated=now())
                continue

            try:
                result = response.json()['result'][-1]
            except (KeyError, IndexError):
                print response.json()
                continue

            log = Log.objects.get(pk=sms_sending.id)

            print result  # TODO: Check this response
            if result['status'] == 'SENT_OK':
                status = SMS_STATUS.SENT
                quantity = result['succ_count']
                port = result['port']
                print log.port, port
                db_port = Port.objects.get(gateway_id=sms_sending.gateway_id, number=port)
                db_port.limit -= quantity
                package = SmsPlanCustomer.objects.get(id=sms_sending.package.id)
                package.left -= quantity
                package.save()
            elif result['status'] == 'SENDING':
                continue
            else:
                status = SMS_STATUS.FAILED
                quantity = None
            log.status = status
            log.updated = now()
            log.quantity = quantity
            log.save()

        for sms_queued in Log.objects.select_related().filter(status=SMS_STATUS.QUEUED)[:20]:
            data = dict(
                text=sms_queued.message,
                port=[sms_queued.port],
                param=[dict(number=sms_queued.phone, text_param=[], user_id=sms_queued.sms_id)]
            )
            session.auth = (sms_queued.gateway.username, sms_queued.gateway.password)

            url = 'http://{}/api/send_sms'.format(sms_queued.gateway.hostname)
            try:
                response = session.post(url, data=dumps(data), headers=headers)
            except ConnectionError:
                # TODO: Update Queue
                continue

            if response.status_code != requests.codes.ok:
                # TODO: Report error to gateway
                continue

            # TODO: Check this response
            # response.json(): {u'sms_in_queue': 1, u'error_code': 202, u'task_id': 0}
            Log.objects.filter(id=sms_queued.id).update(status=SMS_STATUS.SENDING, sent=now())
