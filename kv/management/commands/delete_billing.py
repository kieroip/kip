import pymssql
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Delete SQL for a given Remission in Ofimatica'

    def handle(self, *args, **options):
        with pymssql.connect("192.168.0.107", "KIERO_TEL", "directortelefonia", "KIEROIP").cursor() as cursor:
            remission = "160222"

            delete_queries = [
                "DELETE FROM [KIEROIP].[dbo].[tblMVFACTURAS_IMP] WHERE REMISION LIKE '%{}'".format(remission),
                "DELETE FROM [KIEROIP].[dbo].[tblCALLS_IMP] WHERE REMISION LIKE '%{}'".format(remission),
                "DELETE FROM [KIEROIP].[dbo].[tblFACTURAS_IMP] WHERE REMISION LIKE '%{}'".format(remission)
            ]

            for query in delete_queries:
                cursor.execute(query)
            cursor.connection.commit()
