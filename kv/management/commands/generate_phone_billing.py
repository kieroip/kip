from datetime import date, datetime

import pymssql
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.db import connections

from kv.models import PbxVirtual


class Command(BaseCommand):
    help = 'Generate SQL for Ofimatica'

    def add_arguments(self, parser):
        parser.add_argument('month', type=str)

    @staticmethod
    def date_range(month):
        year = date.today().year
        final_date = str(year) + "-" + month + "-21"

        i_date = datetime.strptime(final_date, "%Y-%m-%d")
        init_date = i_date - relativedelta(months=1)

        init_date = init_date.strftime("%Y-%m-%d")
        return init_date, final_date

    def handle(self, *args, **options):
        month = options['month']

        init_date, final_date = self.date_range(month)

        # Data
        sql_mv = []
        sql_fact = []
        sql_calls = []
        sql_del = []

        with connections['a2b'].cursor() as cursor:
            today = date.today()
            today_ymd = today.strftime('%y%m%d')
            today_datetime = today.strftime('%Y%m%d %H:%M:%S')
            hour = datetime.now().strftime('%H.%M')
            iva = 16

            did_channel_product = 'S14353827'
            did_quantity = 1

            type_fv = 'FV'
            cod_cc = '110102'
            today_10_days = (today + relativedelta(days=10)).strftime('%Y%m%d %H:%M:%S')
            user_fact = 'KieroIP'
            type_vta = '01'

            cursor.execute(
                'SELECT '
                'user.id, user.username, user.id_didgroup, user.zipcode, user.fax, '
                'tariff.tariffgroupname, tariff.additional_code, tariff.included_minutes, tariff.valor_minuto '
                'FROM cc_card AS user '
                'LEFT JOIN cc_tariffgroup AS tariff ON user.tariff = tariff.id '
                'WHERE user.status = 1 '
                'AND user.creationdate < %s '
                'AND user.id NOT IN %s',
                [init_date, (44, 46, 47, 89, 106, 111, 112, 128, 129, 131, 132, 137)]
            )
            for user in cursor.fetchall():
                user_id, user_account, id_didgroup, nit, discount, plan, additional_code, included_min, min_cost = user

                remission_plan = '{}{}'.format(user_id, today_ymd)
                did_brute_total = did_discount = channel_quantity = extra_brute = extra_quantity = 0

                if id_didgroup != -1:  # If -1 there is no DID
                    cursor.execute(
                        'SELECT did, fixrate, max_concurrent '
                        'FROM cc_did '
                        'WHERE iduser = %s '
                        'ORDER BY did ASC',
                        [user_id]
                    )
                    for did_channel in cursor.fetchall():
                        did, fix_rate, max_concurrent = did_channel

                        did_number = 'DID: {}'.format(did)
                        did_value = fix_rate
                        did_brute = round(did_value / 1.16, 2)

                        did_brute_total += did_brute
                        channel_quantity = max_concurrent

                        sql_mv.append(
                            "INSERT INTO tblMVFACTURAS_IMP (REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO) "
                            "VALUES ('{}', '{}', '{}', {}, {}, {}, {});".format(
                                remission_plan, did_channel_product, did_number, did_quantity, did_brute, iva,
                                did_discount
                            )
                        )

                try:
                    pbx = PbxVirtual.objects.get(customer=user_id)
                except ObjectDoesNotExist:
                    pbx_brute_total = 0.0
                else:
                    pbx_brute = round(5000 / 1.16, 2)
                    pbx_brute_total = pbx_brute * pbx.channels
                    sql_mv.append(
                        "INSERT INTO tblMVFACTURAS_IMP (REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO) "
                        "VALUES ('{}', '{}', '{}', {}, {}, {}, {});".format(
                            remission_plan, pbx.code, 'PBX Virtual', pbx.channels, pbx_brute, iva, 0.0
                        )
                    )

                cursor.execute(
                    'SELECT SUM(buycost), SUM( CEIL( real_sessiontime / 60 ) ) '
                    'FROM cc_call '
                    'WHERE LENGTH( dnid ) > 10 '
                    'AND dnid LIKE "00%%" '
                    'AND id_card_package_offer IS NULL '
                    'AND real_sessiontime >= 3 '
                    'AND starttime > %s '
                    'AND starttime < %s '
                    'AND card_id = %s',
                    [init_date, final_date, user_id]
                )
                international_value_net, international_min = cursor.fetchone()
                if international_min:
                    a2b_id = '{}-{}'.format(user_account, today)
                    international_value = round(float(international_value_net) / 1.16, 2)


                    cursor.execute(
                        'SELECT cc_call.destination, cc_prefix.destination, dnid, real_sessiontime, '
                        'buycost/CEIL(real_sessiontime/60), CEIL(real_sessiontime/60) '
                        'FROM cc_call '
                        'INNER JOIN cc_prefix ON cc_call.destination = cc_prefix.prefix '
                        'WHERE LENGTH( dnid ) > 10 '
                        'AND dnid like "00%%" '
                        'AND id_card_package_offer IS NULL '
                        'AND real_sessiontime >= 3 '
                        'AND starttime > %s '
                        'AND starttime < %s '
                        'AND card_id = %s',
                        [init_date, final_date, user_id]
                    )
                    for international_group in cursor.fetchall():
                        destination_id, country, dnid, duration, international_cost_min, billed_min = international_group

                        sql_calls.append(
                            "INSERT INTO tblCALLS_IMP "
                            "(REMISION, FECHA, HORA, IDDESTINO, NOMBRE, NUMERO, DURACION, VALOR, MINUTOS, ID_A2B) "
                            "VALUES ('{}', '{}', '{}', {}, '{}', {}, {}, {}, {}, '{}');".format(
                                remission_plan, today_datetime, hour, destination_id, country, dnid, duration,
                                international_cost_min, billed_min, a2b_id
                            )
                        )
                else:
                    international_min = international_value = 0

                cursor.execute(
                    'SELECT SUM( CEIL( real_sessiontime / 60 ) ) '
                    'FROM cc_call '
                    'WHERE LENGTH( dnid ) > 7 '
                    'AND LENGTH( dnid ) < 11 '
                    'AND id_card_package_offer IS NULL '
                    'AND real_sessiontime >= 3 '
                    'AND starttime > %s '
                    'AND starttime < %s '
                    'AND card_id = %s',
                    [init_date, final_date, user_id]
                )

                min_others = cursor.fetchone()[0] or 0

                cursor.execute(
                    'SELECT SUM( CEIL( real_sessiontime / 60 ) ) '
                    'FROM cc_call '
                    'WHERE LENGTH( dnid ) = 7 '
                    'AND LENGTH( dnid ) < 11 '
                    'AND id_card_package_offer IS NULL '
                    'AND real_sessiontime >= 3 '
                    'AND starttime > %s '
                    'AND starttime < %s '
                    'AND card_id = %s',
                    [init_date, final_date, user_id]
                )

                min_local = cursor.fetchone()[0] or 0

                min_brute = round(min_cost / 1.16, 2)
                if min_others > included_min:
                    extra_quantity = min_others - included_min
                    extra_brute = min_brute * float(extra_quantity)

                channel_net = 27000 if user_id != 61 else 17400
                if id_didgroup < 0:
                    channel_brute = 0
                    min_others += min_local
                else:
                    channel_brute = round(channel_net / 1.16, 2)

                brute = min_brute * included_min + extra_brute + international_value + did_brute_total + (
                    channel_quantity * channel_brute) + pbx_brute_total
                brute_discount = round(brute * discount / 100, 2)
                iva_brute = round((brute - brute_discount) * 0.16, 2)
                note = 'Consumo {} --- Consumo DID {}'.format(min_others, min_local)

                sql_fact.append(
                    "INSERT INTO tblFACTURAS_IMP "
                    "(REMISION, TIPO, BRUTO, CODCC, FECHA, FECHA1, DESCUENTO, NIT, HORA, IVA, NOTA, USUARIO, TIPOVTA) "
                    "VALUES ('{}', '{}', {}, '{}', '{}', '{}', {}, '{}', {}, {}, '{}', '{}', '{}');".format(
                        remission_plan, type_fv, round(brute, 2), cod_cc, today_datetime, today_10_days,
                        brute_discount, nit, hour, iva_brute, note, user_fact, type_vta
                    )
                )

                product_plan = plan[:9]
                plan_name = plan[10:]
                plan_min = min_others - extra_quantity

                sql_mv.append(
                    "INSERT INTO tblMVFACTURAS_IMP "
                    "(REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO, CONSUMO, CONSUMODID) "
                    "VALUES ('{}', '{}', '{}', {}, {}, {}, {}, {}, {});".format(
                        remission_plan, product_plan, plan_name, included_min, min_brute, iva,
                        discount, plan_min, min_local
                    )
                )

                if international_min > 0:
                    international_product = 'S14353859'
                    international_name = 'Consumo internacional'
                    international_quantity = 1
                    international_discount = 0
                    international_did_consumption = 0

                    sql_mv.append(
                        "INSERT INTO tblMVFACTURAS_IMP "
                        "(REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO, CONSUMO, CONSUMODID) "
                        "VALUES ('{}', '{}', '{}', {}, {}, {}, {}, {}, {});".format(
                            remission_plan, international_product, international_name, international_quantity,
                            international_value, iva, international_discount, international_min,
                            international_did_consumption
                        )
                    )

                if channel_quantity > 0:
                    channel_product = 'S14353828'
                    channel_name = 'Canales de acceso'
                    channel_discount = 0

                    sql_mv.append(
                        "INSERT INTO tblMVFACTURAS_IMP (REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO) "
                        "VALUES ('{}', '{}', '{}', {}, {}, {}, {});".format(
                            remission_plan, channel_product, channel_name, channel_quantity, channel_brute, iva,
                            channel_discount
                        )
                    )

                if min_others > included_min:
                    extra_name = 'Consumo adicional'
                    did_consume = 0

                    sql_mv.append(
                        "INSERT INTO tblMVFACTURAS_IMP "
                        "(REMISION, PRODUCTO, NOMBRE, CANTIDAD, VALOR, IVA, DESCUENTO, CONSUMO, CONSUMODID) "
                        "VALUES ('{}', '{}', '{}', {}, {}, {}, {}, {}, {});".format(
                            remission_plan, additional_code, extra_name, extra_quantity, min_brute, iva,
                            discount, extra_quantity, did_consume
                        )
                    )

            sql_del.append("DELETE FROM [KIEROIP].[dbo].[tblMVFACTURAS_IMP] WHERE PRODUCTO LIKE 'Kiero_D';")
            sql_del.append("DELETE FROM [KIEROIP].[dbo].[tblMVFACTURAS_IMP] WHERE PRODUCTO = '0';")

        with pymssql.connect("192.168.0.107", "KIERO_TEL", "directortelefonia", "KIEROIP").cursor() as cursor:
            for queries in (sql_fact, sql_mv, sql_calls, sql_del):
                for query in queries:
                    if 'None' in query:
                        print query
                        continue
                    cursor.execute(query)
            cursor.connection.commit()
