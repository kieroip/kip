import json
import subprocess

from a2b.models import CcCard

from django.core.management.base import NoArgsCommand


class Command(NoArgsCommand):
    help = 'Report for client trunks and their statuses'

    def handle_noargs(self, **options):
        asterisk_command = 'echo "abcd1234" | sudo -S asterisk -rx "sip show peers"'
        p = subprocess.Popen(asterisk_command, stdout=subprocess.PIPE, shell=True)
        output, err = p.communicate()

        result = []

        for trunk in output.split("\n")[1:-2]:
            info = trunk.strip().split(' ')
            try:
                client = CcCard.objects.get(username=info[0].split('/')[0])
            except:
                continue

            status = info[-3] if 'ms' in info[-1] else info[-1]
            result.append(dict(client=client.company_name, status=status))

        return json.dumps(result)
