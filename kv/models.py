from django.db import models
from django.utils.timezone import now

from a2b.models import CcCard
from kv import constants


class Gateway(models.Model):
    name = models.CharField(max_length=50)
    hostname = models.CharField(max_length=50)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Port(models.Model):
    number = models.PositiveSmallIntegerField()
    limit = models.PositiveSmallIntegerField(default=100)

    gateway = models.ForeignKey(Gateway)

    is_active = models.BooleanField(default=True)


class SmsPlan(models.Model):
    name = models.CharField(max_length=50)
    price = models.FloatField()
    bucket = models.PositiveSmallIntegerField()

    is_opened = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class SmsPlanCustomer(models.Model):
    plan = models.ForeignKey(SmsPlan)
    customer = models.PositiveSmallIntegerField()

    left = models.SmallIntegerField(null=True)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return u"{} - {}".format(self.plan, self.customer_name)

    @property
    def customer_name(self):
        return CcCard.objects.get(pk=self.customer).lastname


class Log(models.Model):
    package = models.ForeignKey(SmsPlanCustomer)

    gateway = models.ForeignKey(Gateway)
    sms_id = models.PositiveIntegerField()
    port = models.PositiveSmallIntegerField(null=True)

    phone = models.CharField(max_length=20)
    message = models.CharField(max_length=670)

    queued = models.DateTimeField(default=now)
    sent = models.DateTimeField(null=True)
    updated = models.DateTimeField(null=True)

    quantity = models.PositiveSmallIntegerField(null=True)
    status = models.PositiveSmallIntegerField(default=constants.SMS_STATUS.QUEUED)

    def save(self, *args, **kwargs):
        top = Log.objects.filter(gateway=self.gateway).order_by('-sms_id')
        self.sms_id = top[0].sms_id + 1 if top and top[0].sms_id < 10**6 else 1

        super(Log, self).save(*args, **kwargs)


class InternetPlan(models.Model):
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    price = models.FloatField()
    bucket = models.PositiveSmallIntegerField()
    discount = models.FloatField(default=0.)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class InternetPlanCustomer(models.Model):
    plan = models.ForeignKey(InternetPlan)
    customer = models.PositiveSmallIntegerField()

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return u"{} - {}".format(self.plan, self.customer_name)

    @property
    def customer_name(self):
        return CcCard.objects.get(pk=self.customer).lastname


class PbxVirtual(models.Model):
    code = models.CharField(max_length=20, default="S14353873")
    customer = models.PositiveSmallIntegerField()
    channels = models.PositiveSmallIntegerField()

    is_active = models.BooleanField(default=True)

    @property
    def customer_name(self):
        return CcCard.objects.get(pk=self.customer).lastname

    @property
    def customer_nit(self):
        return CcCard.objects.get(pk=self.customer).zipcode

    @property
    def pbx_cost(self):
        return 5000 * self.channels
