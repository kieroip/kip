from django.conf.urls import url

from kv import views

urlpatterns = [
    url(r'^login/$', views.customer_login, name='customer_login'),
    url(r'^logout/$', views.customer_logout, name='customer_logout'),
    url(r'^sms_log/$', views.sms_log, name='sms_log'),
    url(r'^sms_send/$', views.sms_send, name='sms_send'),
    url(r'^api/send/$', views.send_sms_api, name='send_sms_api'),
    url(r'^kv_log/$', views.kv_log, name='kv_log'),
    url(r'^kv_info/$', views.kv_info, name='kv_info'),
    url(r'^trunks/$', views.trunk_status, name='trunk_status'),
    url(r'^retention/$', views.retention, name='retention'),
]
