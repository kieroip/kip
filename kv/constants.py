# noinspection PyClassHasNoInit
class SMS_STATUS:
    QUEUED = 0
    SENDING = 1
    SENT = 2
    FAILED = 3


# noinspection PyClassHasNoInit
class CALL_STATUS:
    ANSWER = 1
    BUSY = 2
    NOANSWER = 3
    CANCEL = 4
    CONGESTION = 5
    CHANUNAVAIL = 6
