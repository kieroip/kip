from django.contrib import admin

from kv.forms import SmsPlanCustomerForm, InternetPlanCustomerForm, PbxVirtualForm
from kv.models import Gateway, SmsPlan, SmsPlanCustomer, InternetPlan, InternetPlanCustomer, PbxVirtual, Port


class PortInlineAdmin(admin.StackedInline):
    model = Port


@admin.register(Gateway)
class GatewayAdmin(admin.ModelAdmin):
    list_display = ('name', 'hostname', 'username', 'is_active')
    inlines = [PortInlineAdmin]


@admin.register(SmsPlan)
class SmsPlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'bucket')


@admin.register(SmsPlanCustomer)
class SmsPlanCustomerAdmin(admin.ModelAdmin):
    form = SmsPlanCustomerForm
    list_display = ('plan', 'customer_name', 'left')


@admin.register(InternetPlan)
class InternetPlanAdmin(admin.ModelAdmin):
   list_display = ('code', 'name', 'price', 'bucket', 'discount')


@admin.register(InternetPlanCustomer)
class InternetPlanCustomerAdmin(admin.ModelAdmin):
   form = InternetPlanCustomerForm
   list_display = ('plan', 'customer_name')


@admin.register(PbxVirtual)
class PbxVirtualAdmin(admin.ModelAdmin):
    form = PbxVirtualForm
    list_display = ('code', 'customer_name', 'channels', 'pbx_cost')
