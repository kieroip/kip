# coding=utf-8
import json
import urllib
from datetime import date
from math import ceil
from random import choice

from StringIO import StringIO
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from a2b.models import CcCard, CcCall, CcDid, CcTariffgroup, CcCardGroup
from kv import constants
from kv.constants import SMS_STATUS
from kv.forms import CDRFilterForm
from kv.models import SmsPlanCustomer, Log, Gateway, Port


def customer_login(request):
    if request.method == 'POST':
        a2b_username = request.POST.get('username')
        a2b_password = request.POST.get('password')

        a2b_customer = authenticate(username=a2b_username, password=a2b_password)
        if a2b_customer is not None:
            # TODO: Check if customer is not activated.
            login(request, user=a2b_customer)
            return HttpResponseRedirect(request.GET.get('next') or reverse('kv_info'))

    return render(request, 'login.html')


def customer_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('customer_login'))


@csrf_exempt
@require_http_methods(["POST"])
def send_sms_api(request):
    a2b_username = request.POST.get('username')
    a2b_password = request.POST.get('password')

    phones = request.POST.getlist('phones')
    message = request.POST.get('message')

    is_success, text = queue_sms(a2b_username, a2b_password, phones, message)

    return JsonResponse(dict(error=not is_success, message=text))


def queue_sms(a2b_user, a2b_pass, phones, message):
    """
    Queue messages to be processed and sent by a cron task.
    :param a2b_user: a2billing username.
    :param a2b_pass: a2billing password.
    :param phones: list of phones to send the message.
    :param message: text message.
    :return: (success, message) Return if messages were successfully queued and a related message.
    """
    for i in ('', None):
        if i in (a2b_user, a2b_pass, phones, message):
            return False, 'Por favor ingrese todos los parámetros.'

    try:
        customer = CcCard.objects.get(useralias=a2b_user, uipass=a2b_pass)
    except ObjectDoesNotExist:
        return False, 'Credenciales inválidas.'

    # A customer has only one sms plan which can be opened or closed.
    try:
        package = SmsPlanCustomer.objects.select_related().filter(customer=customer.id, is_active=True)[0]
    except IndexError:
        return False, 'Usuario sin plan de mensajería.'

    queued = Log.objects.filter(package=package.id, status=constants.SMS_STATUS.QUEUED)
    left = package.left - queued.count()

    if left <= 0 and not package.plan.is_opened:
        return False, 'Ha alcanzado el límite de envío.'

    for phone in phones:
        if not phone:  # From google file this can take all rows.
            continue

        port = None
        while port is None:
            port = choice(Port.objects.filter(gateway__is_active=True, is_active=True, limit__gt=0).select_related())
            queued_on_port = Log.objects.filter(package=package.id, status=constants.SMS_STATUS.QUEUED, port=port.id).count()
            if port.limit - queued_on_port > 0:
                port = port

        # gateway = choice(Gateway.objects.filter(is_active=True))

        Log(package=package, gateway=port.gateway, phone=phone, port=port.number, message=message).save()

        left -= 1
        if left == 0 and not package.plan.is_opened:
            break

    return True, 'Mensajes programados satisfactoriamente.'


@login_required(login_url=reverse_lazy('customer_login'))
def sms_send(request):
    if request.method == 'POST':
        a2b_username = request.POST.get('username', request.user.username)
        a2b_password = request.POST.get('password', request.user.password)

        message = request.POST.get('message')

        # noinspection PyUnresolvedReferences
        import pyexcel.ext.xlsx
        phones_file = request.FILES.get('phones')
        phones = phones_file.get_sheet().column[0] if phones_file else None

        is_success, text = queue_sms(a2b_username, a2b_password, phones, message)

        level = messages.SUCCESS if is_success else messages.ERROR
        messages.add_message(request, level, text)

    return render(request, 'sms_send.html', {'page': 'sms_send'})


@login_required(login_url=reverse_lazy('customer_login'))
def sms_log(request):
    customer = CcCard.objects.get(useralias=request.user.username, uipass=request.user.password)
    logs = Log.objects.filter(package__customer=customer.id)

    return render(request, 'sms_log.html', {'logs': logs})


@login_required(login_url=reverse_lazy('customer_login'))
def kv_info(request):
    customer = CcCard.objects.get(useralias=request.user.username, uipass=request.user.password)
    sms = SmsPlanCustomer.objects.select_related().filter(customer=customer.id)
    plan = CcTariffgroup.objects.get(id=customer.tariff)
    plan_type = CcCardGroup.objects.get(id=customer.id_group)
    lines = CcDid.objects.filter(iduser=customer.id)

    dids = ','.join([did.did for did in lines])
    minutes = ceil(abs(customer.credit / plan.valor_minuto))
    sms = sms[0] if sms.count() > 0 else None
    # TODO: When the bucket restart?
    sent = Log.objects.filter(package=sms, status=SMS_STATUS.SENT).count()

    return render(request, 'kv_info.html', {
        'page': 'kv_info',
        'customer': customer,
        'sms': sms,
        'plan': plan,
        'plan_type': plan_type,
        'dids': dids,
        'minutes': minutes,
        'sent': sent
    })


@login_required(login_url=reverse_lazy('customer_login'))
def kv_log(request):
    customer = CcCard.objects.get(useralias=request.user.username, uipass=request.user.password)
    logs = CcCall.objects.filter(card_id=customer.id)

    if request.method == 'POST':
        form = CDRFilterForm(request.POST or None)
    else:
        today = date.today()
        form = CDRFilterForm(dict(date_until=today, date_from=today + relativedelta(months=-1)))

    if form.is_valid():
        fields = form.cleaned_data
        date_from = fields.get('date_from')
        date_until = fields.get('date_until')
        destination = fields.get('destination')
        status = fields.get('status')

        if date_from:
            logs = logs.filter(starttime__gte=date_from)
        if date_until:
            logs = logs.filter(starttime__lte=date_until)
        if destination:
            logs = logs.filter(calledstation__contains=destination)
        if status:
            logs = logs.filter(terminatecauseid=status)

    return render(request, 'kv_log.html', {'form': form, 'logs': logs})


def trunk_status(request):
    result = StringIO()
    call_command('get_trunk_statuses', stdout=result)

    return render(request, 'trunk_status.html', {'statuses': eval(result.getvalue())})


def retention(request):
    if request.method == 'POST':
        id = request.POST.get('id')

        try:
            pdf = open('../retention/{}.pdf'.format(id))
            response = HttpResponse(pdf.read(), content_type='application/pdf')
            response['Content-Disposition'] = 'inline; filename="{}.pdf"'.format(id)
            return response
        except IOError:
            messages.add_message(request, messages.ERROR, 'No existe retención para el documento ingresado')
            return render(request, 'retention.html')
    else:
        return render(request, 'retention.html')
