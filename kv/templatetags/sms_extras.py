# coding=utf-8
from math import ceil

from django import template

from kv import constants

register = template.Library()


@register.filter
def status_name(value):
    sms_statuses = {
        constants.SMS_STATUS.QUEUED: 'En espera',
        constants.SMS_STATUS.SENDING: 'Procesando',
        constants.SMS_STATUS.SENT: 'Enviado',
        constants.SMS_STATUS.FAILED: 'Fallido'
    }

    return sms_statuses[value]


@register.filter
def status_class(value):
    sms_classes = {
        constants.SMS_STATUS.QUEUED: 'primary',
        constants.SMS_STATUS.SENDING: 'info',
        constants.SMS_STATUS.SENT: 'success',
        constants.SMS_STATUS.FAILED: 'danger'
    }

    return sms_classes[value]


@register.filter
def call_status_name(value):
    call_statuses = {
        constants.CALL_STATUS.ANSWER: 'Contestada',
        constants.CALL_STATUS.BUSY: 'Ocupado',
        constants.CALL_STATUS.NOANSWER: 'No contestada',
        constants.CALL_STATUS.CANCEL: 'Cancelada',
        constants.CALL_STATUS.CONGESTION: 'Congestión',
        constants.CALL_STATUS.CHANUNAVAIL: 'No disponible',
    }

    return call_statuses[value]


@register.filter
def call_status_class(value):
    call_classes = {
        constants.CALL_STATUS.ANSWER: 'success',
        constants.CALL_STATUS.BUSY: 'default',
        constants.CALL_STATUS.NOANSWER: 'info',
        constants.CALL_STATUS.CANCEL: 'danger',
        constants.CALL_STATUS.CONGESTION: 'primary',
        constants.CALL_STATUS.CHANUNAVAIL: 'warning',
    }

    return call_classes[value]


@register.filter
def secs_to_mins(value):
    return int(ceil(value / 60.))


@register.filter
def trunk_status_class(value):
    statuses_classes = {
        'OK': 'success',
        'UNREACHABLE': 'danger',
        'LAGGED': 'warning',
        'UNKNOWN': 'default',
        'Unmonitored': 'info'
    }

    return statuses_classes[value]
