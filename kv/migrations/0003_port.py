# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-09 15:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kv', '0002_pbxvirtual'),
    ]

    operations = [
        migrations.CreateModel(
            name='Port',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveSmallIntegerField()),
                ('limit', models.PositiveSmallIntegerField(default=100)),
                ('is_active', models.BooleanField(default=True)),
                ('gateway', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kv.Gateway')),
            ],
        ),
    ]
