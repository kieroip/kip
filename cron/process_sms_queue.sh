#!/bin/bash

# Intended to run each minute

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

workon kip

cd $HOME/kip
./manage.py update_sms_queue >> /var/log/cron/sms_queue.log 2>> /var/log/cron/sms_queue.err
